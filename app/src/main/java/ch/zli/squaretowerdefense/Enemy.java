package ch.zli.squaretowerdefense;

import java.util.ArrayList;
import java.util.Random;

public class Enemy {
    public double id;
    public int lives;
    public int mapPart;
    public double coordinateX;
    public double coordinateY;
    public EnemyData data;
    public ArrayList<Effect> effects;
    private int effectTickDelay;
    private int speed;
    public boolean alive;
    public String direction;
    public int timeToSpawn;

    public Enemy(String type, int timeToSpawn, ArrayList<MapPart> mapParts) {
        Random random = new Random();
        this.id = random.nextDouble();
        this.data = new EnemyData();
        this.data.setData(type);
        this.mapPart = 1;
        this.lives = this.data.lives;
        this.effects = new ArrayList<>();
        this.effectTickDelay = 0;
        this.speed = this.data.speed;
        this.alive = false;
        this.timeToSpawn = timeToSpawn;

        this.coordinateX = mapParts.get(0).coordinateX;
        this.coordinateY = mapParts.get(0).coordinateY;

        this.direction = mapParts.get(0).direction;
    }

    public boolean walk(ArrayList<MapPart> mapParts, float[] gravity) {
        if(this.alive) {
            float gravityX = gravity[1];
            float gravityY = gravity[0];

            if(gravityX > 10) gravityX = 10.0f;
            if(gravityX < -10) gravityX = -10.0f;
            if(gravityY > 10) gravityY = 10.0f;
            if(gravityY < -10) gravityY = -10.0f;

            gravityX = gravityX / 20;
            gravityY = gravityY / 20;

            if(this.direction.equals("left")) {
                this.coordinateX = this.coordinateX - ((double)this.speed / 150) * (-gravityX + 1);

                // Richtung wechseln (falls nötig)
                if(this.coordinateX < mapParts.get(this.mapPart).coordinateX) {
                    this.coordinateX = mapParts.get(this.mapPart).coordinateX;
                    this.direction = mapParts.get(this.mapPart).direction;
                    this.mapPart++;

                    if(mapParts.size() == this.mapPart) return false; // Ende erreicht (Game Over)
                }
            } else if(this.direction.equals("up")) {
                this.coordinateY = this.coordinateY - ((double)this.speed / 150) * (-gravityY + 1);

                // Richtung wechseln (falls nötig)
                if(this.coordinateY < mapParts.get(this.mapPart).coordinateY) {
                    this.coordinateY = mapParts.get(this.mapPart).coordinateY;
                    this.direction = mapParts.get(this.mapPart).direction;
                    this.mapPart++;

                    if(mapParts.size() == this.mapPart) return false; // Ende erreicht (Game Over)
                }
            } else if(this.direction.equals("right")) {
                this.coordinateX = this.coordinateX + ((double)this.speed / 150) * (gravityX + 1);

                // Richtung wechseln (falls nötig)
                if(this.coordinateX > mapParts.get(this.mapPart).coordinateX) {
                    this.coordinateX = mapParts.get(this.mapPart).coordinateX;
                    this.direction = mapParts.get(this.mapPart).direction;
                    this.mapPart++;

                    if(mapParts.size() == this.mapPart) return false; // Ende erreicht (Game Over)
                }
            } else {
                this.coordinateY = this.coordinateY + ((double)this.speed / 150) * (gravityY + 1);

                // Richtung wechseln (falls nötig)
                if(this.coordinateY > mapParts.get(this.mapPart).coordinateY) {
                    this.coordinateY = mapParts.get(this.mapPart).coordinateY;
                    this.direction = mapParts.get(this.mapPart).direction;
                    this.mapPart++;

                    if(mapParts.size() == this.mapPart) return false; // Ende erreicht (Game Over)
                }
            }
        } else {
            if(timeToSpawn > 0) {
                timeToSpawn--;
                if(timeToSpawn == 0) this.alive = true;
            }
        }
        return true;
    }

    public void damage(int amount) {
        this.lives = this.lives - amount;
        if(this.lives < 1) this.alive = false;
    }
}
