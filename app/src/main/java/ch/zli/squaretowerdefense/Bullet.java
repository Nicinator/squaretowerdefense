package ch.zli.squaretowerdefense;

import java.util.ArrayList;

public class Bullet {
    public BulletData data;
    public double exactCoordinateX;
    public double exactCoordinateY;
    private double velocityX;
    private double velocityY;
    private int lifespan;
    private int pierce;
    public boolean alive;
    public Bullet childBullet;
    public ArrayList<Double> currentlyDamagingEnemies;

    public Bullet(int spawnX, int spawnY, String type, Enemy targetEnemy) {
        this.data = new BulletData();
        this.data.setData(type);

        this.lifespan = this.data.lifespan;
        this.pierce = this.data.pierce;
        this.alive = true;
        this.childBullet = null;
        this.currentlyDamagingEnemies = new ArrayList<>();

        this.exactCoordinateX = spawnX * 150 + 75;
        this.exactCoordinateY = spawnY * 150 + 75;

        // Distanz und Richtung zwischen Kugel und Gegner berechnen
        double targetDifferenceX = Math.abs(this.exactCoordinateX - (targetEnemy.coordinateX * 150 + 75));
        double targetDifferenceY = Math.abs(this.exactCoordinateY - (targetEnemy.coordinateY * 150 + 75));
        double targetDifference = Math.sqrt(Math.pow(targetDifferenceX, 2) + Math.pow(targetDifferenceY, 2));

        // Richtung und Geschwindigkeit aus den Distanzen berechnen
        this.velocityX = ((targetEnemy.coordinateX * 150 + 75) - this.exactCoordinateX) / targetDifference;
        this.velocityY = ((targetEnemy.coordinateY * 150 + 75) - this.exactCoordinateY) / targetDifference;

        // Geschwindigkeit unabhängig von Distanz setzen
        double totalVelocity = Math.sqrt(Math.pow(Math.abs(this.velocityX), 2) + Math.pow(Math.abs(this.velocityY), 2));
        if(totalVelocity > 0) {
            this.velocityX = this.velocityX / totalVelocity;
            this.velocityY = this.velocityY / totalVelocity;
        }

        // Geschwindigkeit an vorgegebenen Wert anpassen
        this.velocityX = this.velocityX * this.data.speed;
        this.velocityY = this.velocityY * this.data.speed;
    }

    public int tick(ArrayList<Enemy> enemies, int money) {
        int returnValue = money;
        if(this.alive) {
            this.exactCoordinateX = this.exactCoordinateX + this.velocityX;
            this.exactCoordinateY = this.exactCoordinateY + this.velocityY;

            this.lifespan--;
            if(this.lifespan == 0) this.alive = false;

            // Collision Detection
            for(int i = 0; i < enemies.size(); i++) {
                if(enemies.get(i).alive) {
                    double enemyX = enemies.get(i).coordinateX * 150;
                    double enemyY = enemies.get(i).coordinateY * 150;
                    double x = this.exactCoordinateX;
                    double y = this.exactCoordinateY;

                    boolean collide = false;

                    // Kollision im Raster
                    if (x > enemyX - this.data.hitRange && x < enemyX + 150 + this.data.hitRange && y > enemyY - this.data.hitRange && y < enemyY + 150 + this.data.hitRange) {
                        // Kollision an den Ecken
                        if (x < enemyX && y < enemyY) {
                            // Oben links
                            double distanceX = Math.abs(x - enemyX);
                            double distanceY = Math.abs(y - enemyY);
                            double totalDistance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2));
                            if (totalDistance <= this.data.hitRange) {
                                collide = true;
                            } else {
                                collide = false;
                            }
                        } else if (x > enemyX + 150 && y < enemyY) {
                            // Oben rechts
                            double distanceX = Math.abs(x - (enemyX + 150));
                            double distanceY = Math.abs(y - enemyY);
                            double totalDistance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2));
                            if (totalDistance <= this.data.hitRange) {
                                collide = true;
                            } else {
                                collide = false;
                            }
                        } else if (x > enemyX + 150 && y > enemyY + 150) {
                            // Unten rechts
                            double distanceX = Math.abs(x - (enemyX + 150));
                            double distanceY = Math.abs(y - (enemyY + 150));
                            double totalDistance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2));
                            if (totalDistance <= this.data.hitRange) {
                                collide = true;
                            } else {
                                collide = false;
                            }
                        } else if (x < enemyX && y > enemyY + 150) {
                            // Unten links
                            double distanceX = Math.abs(x - enemyX);
                            double distanceY = Math.abs(y - (enemyY + 150));
                            double totalDistance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2));
                            if (totalDistance <= this.data.hitRange) {
                                collide = true;
                            } else {
                                collide = false;
                            }
                        } else {
                            collide = true;
                        }
                    }
                    if (collide) {
                        if (!this.currentlyDamagingEnemies.contains(enemies.get(i).id)) {
                            this.currentlyDamagingEnemies.add(enemies.get(i).id);
                            this.damage(enemies.get(i));
                            this.pierce--;
                            returnValue = returnValue + this.data.damage;
                            if(this.pierce == 0) this.alive = false;
                        }
                    } else {
                        if (this.currentlyDamagingEnemies.contains(enemies.get(i).id)) {
                            this.currentlyDamagingEnemies.remove(enemies.get(i).id);
                        }
                    }
                }
            }
        }
        return returnValue;
    }

    private void damage(Enemy enemy) {
        if(this.data.effect != null) {
            // Effekt zum Gegner hinzufügen
        }

        enemy.damage(this.data.damage);
    }
}
