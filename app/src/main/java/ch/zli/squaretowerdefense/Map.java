package ch.zli.squaretowerdefense;

import android.graphics.Point;

import java.util.ArrayList;

public class Map {
    public ArrayList<MapPart> mapParts;
    public ArrayList<Point> blockedCoordinates;

    public Map(String map) {
        mapParts = new ArrayList<>();
        blockedCoordinates = new ArrayList<>();

        if(map.equals("Start")) {
            mapParts.add(new MapPart(0, 0, 5, "down"));
            mapParts.add(new MapPart(0, 5, 3, "right"));
            mapParts.add(new MapPart(3, 5, 4, "up"));
            mapParts.add(new MapPart(3, 1, 2, "right"));
            mapParts.add(new MapPart(5, 1, 2, "up"));
            mapParts.add(new MapPart(5, 0, 5, "right"));
            mapParts.add(new MapPart(10, 0, 3, "down"));
            mapParts.add(new MapPart(10, 3, 2, "right"));
            mapParts.add(new MapPart(11, 3, 3, "down"));
            mapParts.add(new MapPart(11, 6, 6, "left"));
            mapParts.add(new MapPart(5, 6, 2, "down"));
            mapParts.add(new MapPart(5, 7, 1, "down")); // Ende

            calculateBlockedCoordinates();
        }
    }

    private void calculateBlockedCoordinates() {
        for(int i = 0; i < mapParts.size(); i++) {
            for(int j = 0; j < mapParts.get(i).length; j++) {
                if(mapParts.get(i).direction.equals("right")) {
                    blockedCoordinates.add(new Point(mapParts.get(i).coordinateX + j, mapParts.get(i).coordinateY));
                } else if(mapParts.get(i).direction.equals("down")) {
                    blockedCoordinates.add(new Point(mapParts.get(i).coordinateX, mapParts.get(i).coordinateY + j));
                } else if(mapParts.get(i).direction.equals("left")) {
                    blockedCoordinates.add(new Point(mapParts.get(i).coordinateX - j, mapParts.get(i).coordinateY));
                } else if(mapParts.get(i).direction.equals("up")) {
                    blockedCoordinates.add(new Point(mapParts.get(i).coordinateX, mapParts.get(i).coordinateY - j));
                }
            }
        }
    }
}
