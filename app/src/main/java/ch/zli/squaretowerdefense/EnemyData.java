package ch.zli.squaretowerdefense;

import java.util.ArrayList;

public class EnemyData {
    public String name;
    public int speed;
    public int color;
    public int lives;
    public ArrayList<String> immunities;
    public int borderColor;
    public int borderWidth;

    public void setData(String enemy) {
        this.name = enemy;
        this.immunities = new ArrayList<>();
        this.borderColor = 0xFF000000;
        this.borderWidth = 1;

        if(enemy.equals("red")) {
            this.speed = 1;
            this.color = 0xFFFF0000;
            this.lives = 1;
        } else if(enemy.equals("orange")) {
            this.speed = 2;
            this.color = 0xFFFF7700;
            this.lives = 2;
        }
    }
}
