package ch.zli.squaretowerdefense;

public class MapPart {
    public int color;
    public int coordinateX;
    public int coordinateY;
    public int length;
    public String direction;

    public MapPart(int x, int y, int l, String dir) {
        color = 0xffff9a47;
        coordinateX = x;
        coordinateY = y;
        length = l;
        direction = dir;
    }
}
