package ch.zli.squaretowerdefense;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.view.View;

import java.util.ArrayList;

public class CustomCanvas extends View {
    public ArrayList<ShapeDrawable> mapParts;
    public ArrayList<ShapeDrawable> enemies;
    public ArrayList<ShapeDrawable> towers;
    public ArrayList<ShapeDrawable> bullets;

    public CustomCanvas(Context context) {
        super(context);
        setContentDescription(context.getResources().getString(R.string.app_name));

        mapParts = new ArrayList<>();
        enemies = new ArrayList<>();
        towers = new ArrayList<>();
        bullets = new ArrayList<>();
    }

    protected void onDraw(Canvas canvas) {
        for (int i = 0; i < mapParts.size(); i++) {
            mapParts.get(i).draw(canvas);
        }

        for(int i = 0; i < enemies.size(); i++) {
            enemies.get(i).draw(canvas);
        }

        for(int i = 0; i < towers.size(); i++) {
            towers.get(i).draw(canvas);
        }

        for(int i = 0; i < bullets.size(); i++) {
            towers.get(i).draw(canvas);
        }
    }

    // Bewegung der Gegner (für später)
    /*
    protected void run() {
        Rect rect = enemies.get(0).getBounds();
        rect.top++;
        enemies.get(0).setBounds(rect);
    }*/
}
