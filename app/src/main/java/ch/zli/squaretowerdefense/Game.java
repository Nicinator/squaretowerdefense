package ch.zli.squaretowerdefense;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Game extends AppCompatActivity implements SensorEventListener {
    // Canvas
    CustomCanvas canvas;
    ConstraintLayout canvasContainer;
    ConstraintLayout container;
    Handler handler;
    Runnable handlerTask;

    ImageView menuButton;
    String selectedTab;
    TabLayout tabs;

    Map map;
    Level level;

    boolean paused;
    boolean gameOver;

    int MAX_HEIGHT = 7;
    int MAX_WIDTH = 12;

    int money;
    TextView moneySign;

    float[] gravity;
    SensorManager sensorManager;
    Sensor sensor;

    ArrayList<Tower> towers;

    LinearLayout towerOverlay;
    TextView towerOverlayText;
    Button upgradeButton;
    Button sellButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        towerOverlay = findViewById(R.id.towerOverlay);
        towerOverlayText = findViewById(R.id.towerOverlayText);
        upgradeButton = findViewById(R.id.upgradeButton);
        sellButton = findViewById(R.id.sellButton);

        // Canvas
        canvas = new CustomCanvas(this);
        canvas.setOnTouchListener(handleTouch);
        canvasContainer = findViewById(R.id.canvasContainer);
        canvasContainer.addView(canvas, 0);
        container = findViewById(R.id.container);
        container.removeView(canvasContainer);
        setContentView(canvasContainer);

        // Vollbildmodus aktivieren
        hideSystemUI();

        // Map erstellen
        map = new Map("Start");
        drawMapParts();

        selectedTab = "Shooter";

        paused = false;
        gameOver = false;

        money = 50;
        moneySign = findViewById(R.id.moneySign);

        // Schwerkraft
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        gravity = new float[]{0.0f, 0.0f, 0.0f};

        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        towers = new ArrayList<>();

        // Spiel starten
        level = new Level(map.mapParts);

        // Spieltick
        handler = new Handler();
        handlerTask = new Runnable()
        {
            @Override
            public void run() {
                handler.postDelayed(handlerTask, 16);

                if(!paused && !gameOver) {
                    moneySign.setText(money + "$");
                    if(!level.tick(gravity)) {
                        gameOver = true;
                    }
                    for(int i = 0; i < towers.size(); i++) {
                        money = towers.get(i).tick(level.enemies, money);
                    }
                    drawEnemies();
                    drawTowers();
                    canvas.invalidate(); // Alles zeichnen
                }
            }
        };
        handlerTask.run();
    }

    // Click Event
    private View.OnTouchListener handleTouch = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideSystemUI();

            // Position des Klicks ermitteln (für später)
            int x = (int) event.getX();
            int y = (int) event.getY();

            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(x > 1770 && y < 150) {
                    paused = true;
                    setContentView(R.layout.activity_main);

                    initializeComponents();
                    selectedTab = "Shooter";
                    switchTower(); // Beschreibung laden
                }

                if(!paused) {
                    boolean towerPlaced = false;
                    // Turm platzieren
                    x = (int) Math.floor(x / 150);
                    y = (int) Math.floor(y / 150);

                    if(towerOverlay.getVisibility() == View.GONE) {
                        if(x < MAX_WIDTH && y < MAX_HEIGHT) {
                            TowerData data = new TowerData();
                            data.setData(selectedTab, 1, -1);
                            if(money >= data.cost.get(0)) {
                                boolean blocked = false;
                                for(int i = 0; i < map.blockedCoordinates.size(); i++) {
                                    if(map.blockedCoordinates.get(i).x == x && map.blockedCoordinates.get(i).y == y) {
                                        blocked = true;
                                    }
                                }

                                for(int i = 0; i < towers.size(); i++) {
                                    if(towers.get(i).coordinateX == x && towers.get(i).coordinateY == y) {
                                        blocked = true;
                                    }
                                }

                                if(!blocked) {
                                    towers.add(new Tower(selectedTab, x ,y));
                                    money = money - data.cost.get(0);
                                    towerPlaced = true;
                                }
                            }
                        }
                    } else if(towerOverlay.getVisibility() == View.VISIBLE) {
                        towerOverlay.setVisibility(View.GONE);
                    }

                    // Turm-Overlay anzeigen
                    Tower clickedTower = null;
                    for(int i = 0; i < towers.size(); i++) {
                        if(towers.get(i).coordinateX == x && towers.get(i).coordinateY == y) {
                            clickedTower = towers.get(i);
                        }
                    }

                    if(clickedTower != null && !towerPlaced) {
                        String selectedTowerText = "";
                        if(clickedTower.data.name.equals("Shooter")) {
                            selectedTowerText = "Kugelschiesser";
                        } else if(clickedTower.data.name.equals("Bomb")) {
                            selectedTowerText = "Bombenturm";
                        } else if(clickedTower.data.name.equals("Sniper")) {
                            selectedTowerText = "Scharfschütze";
                        }

                        selectedTowerText = selectedTowerText + " Level " + clickedTower.data.level;

                        int moneyBack = 0;
                        for(int i = 0; i < clickedTower.data.level; i++) {
                            moneyBack = moneyBack + clickedTower.data.cost.get(i);
                        }

                        moneyBack = moneyBack / 2;

                        if(clickedTower.data.level != 4) {
                            towerOverlayText.setText(selectedTowerText + "\n\n" +
                                    "Kosten für Upgrade: " + clickedTower.data.cost.get(clickedTower.data.level) + "$\n" +
                                    "Rückerstattung bei Verkauf: " + moneyBack + "$\n");
                        } else {
                            towerOverlayText.setText(selectedTowerText + "\n\n" +
                                    "Spezial: " + clickedTower.data.specialName + "\n" +
                                    "Rückerstattung bei Verkauf: " + moneyBack + "$\n");
                        }

                        towerOverlay.setVisibility(View.VISIBLE);

                        // On Click Listeners
                        int finalMoneyBack = moneyBack;
                        Tower finalClickedTower = clickedTower;
                        View.OnClickListener sellTower = new View.OnClickListener() {
                            public void onClick(View v) {
                                if (v.equals(sellButton)) {
                                    money = money + finalMoneyBack;
                                    towers.remove(finalClickedTower);
                                    towerOverlay.setVisibility(View.GONE);
                                }
                            }
                        };
                        sellButton.setOnClickListener(sellTower);

                        if(clickedTower.data.level != 4) {
                            Tower finalClickedTower1 = clickedTower;
                            View.OnClickListener upgradeTower = new View.OnClickListener() {
                                public void onClick(View v) {
                                    if (v.equals(upgradeButton)) {
                                        money = money - finalClickedTower1.upgrade(money);
                                        towerOverlay.setVisibility(View.GONE);
                                    }
                                }
                            };
                            upgradeButton.setOnClickListener(upgradeTower);
                            upgradeButton.setVisibility(View.VISIBLE);
                        } else {
                            upgradeButton.setVisibility(View.GONE);
                        }
                    }
                }
            }

            return true;
        }
    };

    private void initializeComponents() {
        // Menu Button
        menuButton = findViewById(R.id.menuButton);

        View.OnClickListener clickListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (v.equals(menuButton)) {
                    paused = false;
                    setContentView(canvasContainer);
                }
            }
        };
        menuButton.setOnClickListener(clickListener);

        // Menu
        tabs = findViewById(R.id.tabs);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getContentDescription().toString();
                switchTower();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    // Menu
    private void switchTower() {
        if(selectedTab.equals("Shooter")) {
            TextView text = findViewById(R.id.description);
            text.setText("Kugelschiesser\n\n" +
                    "Der Kugelschiesser verschiesst einfache Kugeln auf die Quadrate. Es können mehrere Quadrate auf ein mal getroffen werden. Die Kugeln sind langsam und machen wenig Schaden. Dafür ist der Kugelschiesser günstig.\n\n" +
                    "Schaden: 1\n" +
                    "Reichweite: 3\n" +
                    "Durchschlagskraft: 2\n" +
                    "Geschossgeschwindigkeit: 2\n" +
                    "Nachladegeschwindigkeit: 3s\n\n" +
                    "Basiskosten: 25$\n" +
                    "Level 2: 100$\n" +
                    "Level 3: 350$\n" +
                    "Spezialupgrade: 500$\n\n" +
                    "Spezialupgrades: Extraschaden, Nachladegeschwindigkeit, Geschossgeschwindigkeit");
        }
    }

    // Vollbildmodus
    private void hideSystemUI() {
        View view = getWindow().getDecorView();
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void drawMapParts() {
        for(int i = 0; i < map.mapParts.size(); i++) {
            ShapeDrawable shape = new ShapeDrawable(new RectShape());
            shape.getPaint().setColor(map.mapParts.get(i).color);

            int inputX = map.mapParts.get(i).coordinateX;
            int inputY = map.mapParts.get(i).coordinateY;
            int inputLength = map.mapParts.get(i).length;

            int x;
            int y;
            int otherX;
            int otherY;

            if(map.mapParts.get(i).direction == "right") {
                x = inputX * 150;
                y = inputY * 150;
                otherX = x + inputLength * 150;
                otherY = y + 150;
            } else if(map.mapParts.get(i).direction == "down") {
                x = inputX * 150;
                y = inputY * 150;
                otherX = x + 150;
                otherY = y + inputLength * 150;
            } else if(map.mapParts.get(i).direction == "left") {
                x = inputX * 150 - inputLength * 150 + 150;
                y = inputY * 150;
                otherX = inputX * 150 + 150;
                otherY = inputY * 150 + 150;
            } else {
                x = inputX * 150;
                y = inputY * 150 - inputLength * 150 + 150;
                otherX = inputX * 150 + 150;
                otherY = inputY * 150 + 150;
            }

            shape.setBounds(x, y, otherX, otherY);
            canvas.mapParts.add(shape);
        }
    }

    private void drawEnemies() {
        canvas.enemies.clear();
        for(int i = 0; i < level.enemies.size(); i++) {
            if(level.enemies.get(i).alive) {
                ShapeDrawable foreground = new ShapeDrawable(new RectShape());
                foreground.getPaint().setColor(level.enemies.get(i).data.color);

                ShapeDrawable background = new ShapeDrawable(new RectShape());
                background.getPaint().setColor(level.enemies.get(i).data.borderColor);

                int x = (int) Math.round(level.enemies.get(i).coordinateX * 150);
                int y = (int) Math.round(level.enemies.get(i).coordinateY * 150);
                int borderWidth = level.enemies.get(i).data.borderWidth;

                background.setBounds(x, y, x + 150, y + 150);
                canvas.enemies.add(background);

                foreground.setBounds(x + borderWidth * 5, y + borderWidth * 5, x + 150 - borderWidth * 5, y + 150 - borderWidth * 5);
                canvas.enemies.add(foreground);
            }
        }
    }

    private void drawTowers() {
        canvas.towers.clear();
        canvas.bullets.clear();
        for(int i = 0; i < towers.size(); i++) {
            ShapeDrawable foreground = new ShapeDrawable(new RectShape());
            foreground.getPaint().setColor(towers.get(i).data.color);

            ShapeDrawable background = new ShapeDrawable(new RectShape());
            background.getPaint().setColor(towers.get(i).data.borderColor);

            int x = (int) Math.round(towers.get(i).coordinateX * 150);
            int y = (int) Math.round(towers.get(i).coordinateY * 150);
            int borderWidth = towers.get(i).data.borderWidth;

            background.setBounds(x, y, x + 150, y + 150);
            canvas.towers.add(background);

            foreground.setBounds(x + borderWidth * 5, y + borderWidth * 5, x + 150 - borderWidth * 5, y + 150 - borderWidth * 5);
            canvas.towers.add(foreground);
        }

        for(int i = 0; i < towers.size(); i++) {
            // Kugeln
            for(int j = 0; j < towers.get(i).bullets.size(); j++) {
                ShapeDrawable bforeground = new ShapeDrawable(new OvalShape());
                bforeground.getPaint().setColor(towers.get(i).bullets.get(j).data.color);

                ShapeDrawable bbackground = new ShapeDrawable(new OvalShape());
                bbackground.getPaint().setColor(towers.get(i).bullets.get(j).data.borderColor);

                int bx = (int) Math.round(towers.get(i).bullets.get(j).exactCoordinateX);
                int by = (int) Math.round(towers.get(i).bullets.get(j).exactCoordinateY);
                int bborderWidth = towers.get(i).bullets.get(j).data.borderWidth;

                bbackground.setBounds(bx - towers.get(i).bullets.get(j).data.size, by - towers.get(i).bullets.get(j).data.size, bx + towers.get(i).bullets.get(j).data.size, by + towers.get(i).bullets.get(j).data.size);
                canvas.towers.add(bbackground);

                bforeground.setBounds(bx - towers.get(i).bullets.get(j).data.size + bborderWidth * 2, by - towers.get(i).bullets.get(j).data.size + bborderWidth * 2, bx + towers.get(i).bullets.get(j).data.size - bborderWidth * 2, by + towers.get(i).bullets.get(j).data.size - bborderWidth * 2);
                canvas.towers.add(bforeground);
            }
        }
    }

    // Schwerkraft
    @Override
    public void onSensorChanged(SensorEvent event) {
        gravity[0] = event.values[0];
        gravity[1] = event.values[1];
        gravity[2] = event.values[2];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}