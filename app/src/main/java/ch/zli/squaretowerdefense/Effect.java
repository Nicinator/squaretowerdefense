package ch.zli.squaretowerdefense;

public class Effect {
    public String name;
    public int damage;
    public int interval;
    public int duration;
    public int speedFactor;
    public boolean disableImmunities;
    public int borderColor;
    public int weight;

    public void setData(String effect) {
        this.name = effect;

        if(effect.equals("Fire")) {
            damage = 1;
            interval = 30;
            duration = 240;
            speedFactor = 1;
            disableImmunities = false;
            borderColor = 0xFFFCBA03;
            weight = 10;
        }
    }
}
