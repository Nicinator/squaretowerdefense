package ch.zli.squaretowerdefense;

import java.util.ArrayList;
import java.util.Random;

public class Level {
    public ArrayList<Enemy> enemies;
    public int level;
    public ArrayList<MapPart> mapParts;
    private double difficultyFactor;

    public Level(ArrayList<MapPart> mapParts) {
        this.level = 0;
        this.enemies = new ArrayList<>();
        this.mapParts = mapParts;
        this.difficultyFactor = 3;
        next();
    }

    public void next() {
        this.level++;
        enemies.clear();

        int difficulty = (int) Math.round(Math.pow((this.level + 2), difficultyFactor));
        double maxEnemy = difficulty / 100;

        ArrayList<String> enemyStrings = new ArrayList<>();
        enemyStrings.add("red");
        enemyStrings.add("orange");

        // Gegner generieren
        Random random = new Random();
        while (difficulty > 0) {
            int nextEnemy;
            if(maxEnemy != 0) nextEnemy = (int) Math.round(random.nextDouble() * maxEnemy);
            else nextEnemy = 0;
            if(nextEnemy > enemyStrings.size() - 1) nextEnemy = enemyStrings.size() - 1;
            int maxNumber = 200 - this.level * 2;
            if(maxNumber < 10) maxNumber = 10;
            int timeToSpawn = random.nextInt(maxNumber) + 1;
            if(this.enemies.size() > 0) timeToSpawn = timeToSpawn + this.enemies.get(this.enemies.size() - 1).timeToSpawn;
            else timeToSpawn = 1; // 1 startet sofort, 0 ist bereits gestartet
            Enemy enemy = new Enemy(enemyStrings.get(nextEnemy), timeToSpawn, this.mapParts);
            this.enemies.add(enemy);
            if(nextEnemy == 0) difficulty -= 5;
            if(nextEnemy == 1) difficulty -= 10;
        }
    }

    public boolean tick(float[] gravity) {
        boolean gameRunning = true;
        boolean levelOver = true;
        for(int i = 0; i < this.enemies.size(); i++) {
            if(!this.enemies.get(i).walk(this.mapParts, gravity)) {
                gameRunning = false;
            }
            if(this.enemies.get(i).alive || this.enemies.get(i).timeToSpawn > 0) levelOver = false;
        }

        if(levelOver) this.next();

        return gameRunning;
    }
}
