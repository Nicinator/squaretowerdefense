package ch.zli.squaretowerdefense;

import java.util.ArrayList;

public class TowerData {
    public String name;
    public ArrayList<Integer> cost;
    public int level;
    public String specialName;
    public int special;
    public int reloadSpeed;
    public int range;
    public String bulletType;
    public int color;
    public int borderColor;
    public int borderWidth;

    public void setData(String tower, int level, int special) {
        this.name = tower;
        this.level = level;
        this.special = special;
        this.specialName = "";
        this.borderColor = 0xFF000000;
        this.borderWidth = 1;
        cost = new ArrayList<>();

        if(tower.equals("Shooter") && special != -1) {
            if(special == 0) {
                this.specialName = "Extraschaden";
            } else if(special == 1) {
                this.specialName = "Nachladegeschwindigkeit";
            } else if(special == 2) {
                this.specialName = "Geschossgeschwindigkeit";
            }
        }

        if(tower.equals("Shooter")) {
            cost.add(25);
            cost.add(100);
            cost.add(350);
            cost.add(500);
            this.color = 0xFF693100;
            if(level == 1) {
                this.reloadSpeed = 180;
                this.range = 3;
                this.bulletType = "Shooter1";
            } else if(level == 2) {
                this.reloadSpeed = 120;
                this.range = 3;
                this.bulletType = "Shooter2";
                this.borderColor = 0xFF707070;
            } else if(level == 3) {
                this.reloadSpeed = 90;
                this.range = 4;
                this.bulletType = "Shooter3";
                this.borderColor = 0xFFFFC400;
            } else {
                this.borderWidth = 2;
                if(specialName == "Extraschaden") {
                    this.reloadSpeed = 90;
                    this.range = 4;
                    this.bulletType = "ShooterED";
                    this.borderColor = 0xFFFF2020;
                } else if(specialName == "Nachladegeschwindigkeit") {
                    this.reloadSpeed = 30;
                    this.range = 4;
                    this.bulletType = "ShooterRS";
                    this.borderColor = 0xFF00FF50;
                } else {
                    this.reloadSpeed = 90;
                    this.range = 4;
                    this.bulletType = "ShooterBS";
                    this.borderColor = 0xFF0050FF;
                }
            }
        }
    }
}
