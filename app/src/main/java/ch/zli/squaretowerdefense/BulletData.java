package ch.zli.squaretowerdefense;

public class BulletData {
    public String name;
    public int damage;
    public int lifespan;
    public int speed;
    public int pierce;
    public String childBullet;
    public int color;
    public int borderColor;
    public int borderWidth;
    public int size;
    public int hitRange;
    public String effect;

    public void setData(String name) {
        this.name = name;
        this.borderColor = 0xFF000000;
        this.borderWidth = 1;
        this.childBullet = "none";
        this.effect = null;

        if(name.equals("Shooter1")) {
            this.damage = 1;
            this.lifespan = 240;
            this.speed = 2;
            this.pierce = 2;
            this.color = 0xFFFFF785;
            this.size = 40;
            this.hitRange = 37;
        } else if(name.equals("Shooter2")) {
            this.damage = 2;
            this.lifespan = 240;
            this.speed = 2;
            this.pierce = 3;
            this.color = 0xFFFFF785;
            this.size = 40;
            this.hitRange = 37;
            this.borderColor = 0xFF707070;
        } else if(name.equals("Shooter3")) {
            this.damage = 2;
            this.lifespan = 240;
            this.speed = 3;
            this.pierce = 5;
            this.color = 0xFFFFF785;
            this.size = 40;
            this.hitRange = 37;
            this.borderColor = 0xFFFFC400;
        } else if(name.equals("ShooterED")) {
            this.damage = 6;
            this.lifespan = 240;
            this.speed = 3;
            this.pierce = 5;
            this.color = 0xFFFFF785;
            this.size = 40;
            this.hitRange = 37;
            this.borderColor = 0xFFFF2020;
            this.borderWidth = 2;
        } else if(name.equals("ShooterRS")) {
            this.damage = 2;
            this.lifespan = 240;
            this.speed = 3;
            this.pierce = 5;
            this.color = 0xFFFFF785;
            this.size = 40;
            this.hitRange = 37;
            this.borderColor = 0xFF00FF50;
            this.borderWidth = 2;
        } else if(name.equals("ShooterBS")) {
            this.damage = 2;
            this.lifespan = 180;
            this.speed = 6;
            this.pierce = 5;
            this.color = 0xFFFFF785;
            this.size = 40;
            this.hitRange = 37;
            this.borderColor = 0xFF0050FF;
            this.borderWidth = 2;
        }
    }
}
