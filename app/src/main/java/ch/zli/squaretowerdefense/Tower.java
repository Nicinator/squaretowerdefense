package ch.zli.squaretowerdefense;

import java.util.ArrayList;
import java.util.Random;

public class Tower {
    public TowerData data;
    public int coordinateX;
    public int coordinateY;
    private int reloadDelay;
    public ArrayList<Bullet> bullets;

    public Tower(String type, int x, int y) {
        this.bullets = new ArrayList<>();
        this.data = new TowerData();
        this.data.setData(type, 1, -1);
        this.coordinateX = x;
        this.coordinateY = y;
        this.reloadDelay = 0;
    }

    public int tick(ArrayList<Enemy> enemies, int money) {
        this.reloadDelay--;
        if(this.reloadDelay < 0) {
            ArrayList<Enemy> enemiesInRange = new ArrayList<>();
            for(int i = 0; i < enemies.size(); i++) {
                if(enemies.get(i).alive) {
                    double distanceX = Math.abs(this.coordinateX - enemies.get(i).coordinateX);
                    double distanceY = Math.abs(this.coordinateY - enemies.get(i).coordinateY);
                    double distance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

                    if(distance <= this.data.range) {
                        enemiesInRange.add(enemies.get(i));
                    }
                }
            }

            // Letzter Gegner nach Streckenabteil
            int highestMapPart = 0;
            ArrayList<Enemy> enemiesOnHighestMapPart = new ArrayList<>();
            for(int i = 0; i < enemiesInRange.size(); i++) {
                if(enemiesInRange.get(i).mapPart == highestMapPart) {
                    highestMapPart = enemiesInRange.get(i).mapPart;
                    enemiesOnHighestMapPart.add(enemiesInRange.get(i));
                } else if(enemiesInRange.get(i).mapPart >= highestMapPart) {
                    enemiesOnHighestMapPart.clear();
                    highestMapPart = enemiesInRange.get(i).mapPart;
                    enemiesOnHighestMapPart.add(enemiesInRange.get(i));
                }
            }

            // Letzter Gegner
            Enemy farestEnemy = null;
            if(enemiesOnHighestMapPart.size() > 1) {
                double bestScore;
                int winIndex = 0;
                if(enemiesOnHighestMapPart.get(0).direction.equals("left")) {
                    bestScore = 999999;
                    for(int i = 0; i < enemiesOnHighestMapPart.size(); i++) {
                        if(enemiesOnHighestMapPart.get(i).coordinateX < bestScore) {
                            bestScore = enemiesOnHighestMapPart.get(i).coordinateX;
                            winIndex = i;
                        }
                    }
                } else if(enemiesOnHighestMapPart.get(0).direction.equals("right")) {
                    bestScore = -999999;
                    for(int i = 0; i < enemiesOnHighestMapPart.size(); i++) {
                        if(enemiesOnHighestMapPart.get(i).coordinateX > bestScore) {
                            bestScore = enemiesOnHighestMapPart.get(i).coordinateX;
                            winIndex = i;
                        }
                    }
                } else if(enemiesOnHighestMapPart.get(0).direction.equals("up")) {
                    bestScore = 999999;
                    for(int i = 0; i < enemiesOnHighestMapPart.size(); i++) {
                        if(enemiesOnHighestMapPart.get(i).coordinateY < bestScore) {
                            bestScore = enemiesOnHighestMapPart.get(i).coordinateY;
                            winIndex = i;
                        }
                    }
                } else if(enemiesOnHighestMapPart.get(0).direction.equals("down")) {
                    bestScore = -999999;
                    for(int i = 0; i < enemiesOnHighestMapPart.size(); i++) {
                        if(enemiesOnHighestMapPart.get(i).coordinateY > bestScore) {
                            bestScore = enemiesOnHighestMapPart.get(i).coordinateY;
                            winIndex = i;
                        }
                    }
                }
                farestEnemy = enemiesOnHighestMapPart.get(winIndex);
            } else if(enemiesOnHighestMapPart.size() == 1) {
                farestEnemy = enemiesOnHighestMapPart.get(0);
            }
            if(farestEnemy != null) {
                this.bullets.add(new Bullet(this.coordinateX, this.coordinateY, this.data.bulletType, farestEnemy));
                this.reloadDelay = this.data.reloadSpeed;
            }
        }
        int returnValue = money;
        for(int i = 0; i < this.bullets.size(); i++) {
            money = this.bullets.get(i).tick(enemies, money);
        }
        returnValue = money;

        // Zerstörte Kugeln entfernen
        ArrayList<Bullet> aliveBullets = new ArrayList<>();
        for(int i = 0; i < this.bullets.size(); i++) {
            if(this.bullets.get(i).alive) aliveBullets.add(this.bullets.get(i));
        }

        this.bullets = aliveBullets;

        return returnValue;
    }

    public int upgrade(int money) {
        int returnValue = 0;
        if(money >= this.data.cost.get(this.data.level)) {
            returnValue = this.data.cost.get(this.data.level);
            if(this.data.level == 3) {
                // Spezialupgrade
                Random random = new Random();
                this.data.setData(this.data.name, this.data.level + 1, random.nextInt(3));
            } else {
                this.data.setData(this.data.name, this.data.level + 1, -1);
            }
        }
        return returnValue;
    }
}
